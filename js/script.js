const tabs = Array.from(document.getElementsByClassName("types-of-designs"));
const tabsDescr = Array.from(document.getElementsByClassName("text-services"));

document.querySelector(".all-designs").addEventListener("click", function (event) {
  if (event.target.classList.contains("types-of-designs")) {
    tabs.forEach((item) => item.classList.remove("active"));
    event.target.classList.add("active");
    tabsDescr.forEach((item) => {
      item.classList.remove("active");
      if (event.target.dataset.tab === item.dataset.tab)
        item.classList.add("active");
    });
  }
});

document.querySelectorAll('.list img').forEach(function (el) {
  el.addEventListener('click', function () {
    selectPerson(el);
  });
});

function selectPerson(el) {
  let selected = document.querySelector('.selected');
  let div = document.createElement('div');
  div.style.color = "#BBBBBB"; 
  div.style.paddingBottom = "37px";
  div.style.fontFamily = "Montserrat";
  div.style.opacity ="0.5";
  div.style.lineHeight ="25px";
div.setAttribute('data-id', el.getAttribute('data-id'));
  div.innerHTML = el.getAttribute('data-article');
  selected.innerHTML = '';
  selected.appendChild(div);
  let h2 = document.createElement('h2');
  h2.style.color = "#18CFAB"; 
  h2.style.textTransform = "uppercase";
  h2.style.fontWeight ="700";
  h2.style.fontSize ="16px";
  h2.innerHTML = el.getAttribute('data-name');
  selected.appendChild(h2);
  let p = document.createElement('p');
  p.style.color = "#BBBBBB";
  p.style.paddingBottom ="38px";
  p.style.paddingTop ="5px";
  p.style.opacity ="0.5";
  p.innerHTML = el.getAttribute('data-position');
  selected.appendChild(p);
  let img = document.createElement('img');
  img.setAttribute('src', el.getAttribute('src'));
  selected.appendChild(img);
}

document.querySelector('.img-foot').click();

document.querySelector('.buttom-people-right').addEventListener('click', function () {
  let id = parseInt(document.querySelector('.selected > div').getAttribute('data-id'));
  id++;
  if (id > 3) {
    id = 0;
  }
  selectPerson(document.querySelector(`.img-foot[data-id='${id}']`))

});

document.querySelector('.buttom-people-left').addEventListener('click', function () {
  let id = parseInt(document.querySelector('.selected > div').getAttribute('data-id'));
  id--;
  if (id === -1) {
    id = 3;
  }
  selectPerson(document.querySelector(`.img-foot[data-id='${id}']`))
});


const images = {
  type1: [
    {

      url: "<li class='portfolio-img'>" +
        "<img src='./img/graphic%20design/graphic-design1.jpg' style='width: 286px; height: 208px'>" +
        "<div class='portfolio-hover hov-img'>" +
        "   <div class='portfolio-links'>" +
        "<a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p class='portfolio-title'>creative design</p><p>Web design</p></div></div></li>",
      name: 'Type 1 Image 1',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design2.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 2',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design3.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 3',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design4.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 4',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design5.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 5',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design6.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 6',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design7.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 7',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design8.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 8',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design9.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 9',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design10.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 10',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design11.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 11',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design12.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 12',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/graphic%20design/graphic-design8.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 1 Image 13',
      link: '',
    },
  ],
  type2: [
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design1.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 1',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design2.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 2',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design3.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 3',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design4.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 4',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design5.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 5',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design6.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 6',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design7.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 7',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design8.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 8',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design9.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 9',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design10.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 10',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design11.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 11',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design12.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 12',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design13.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 13',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design14.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 14',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design15.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 15',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/web%20design/web-design16.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 2 Image 16',
      link: '',
    },
  ],
  type3: [
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page1.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 1',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page2.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 2',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page3.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 3',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page4.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 4',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page5.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 5',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page6.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 6',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/landing%20page/landing-page7.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 3 Image 7',
      link: '',
    },

  ],
  type4: [
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress1.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 1',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress2.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 2',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress3.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 3',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress4.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 4',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress5.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 5',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress6.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 6',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress7.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 7',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress8.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 8',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress9.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 9',
      link: '',
    },
    {
      url: "<li class='portfolio-img'><img src='./img/wordpress/wordpress10.jpg'" +
        "style='width: 286px; height: 208px'><div class='portfolio-hover hov-img'><div class='portfolio-links'><a href='#'><i class='fas fa-link'></i></a><a href='#'><i class='fas fa-search'></i></a></div><div class='portfolio-info'><p>creative design</p></div></div></li>",
      name: 'Type 4 Image 10',
      link: '',
    },

  ],
};

const TYPES = {
  all: 'all',
  type1: 'type1',
  type2: 'type2',
  type3: 'type3',
  type4: 'type4',
};

const liActiveClass = 'tabs-service-active';
const typeContainer = document.querySelector('.amazing-ul');
const imageContainer = document.querySelector('.images');
const loadMore = document.querySelector('.load-more');
let activeType = TYPES.all;
let perPage = 12;
let activePage = 0;

document.querySelector('.tabs-service[data-service=all]').classList.add(liActiveClass);
typeContainer.addEventListener('click', e => {
  const {target} = e;
  const liList = document.querySelectorAll('.tabs-service');
  liList.forEach(li => li.classList.remove(liActiveClass));
  target.classList.add(liActiveClass);
  if (activeType !== target.dataset.service) {
    activeType = target.dataset.service;
    activePage = 0;
    addimg(target.dataset.service);
  }
});
let lodeQuantity = 0;

function addimg(type) {
  if (images[type]) {
    activePage = activePage + 1;
    const imageByType = [...images[type]].slice(0, activePage * perPage);
    imageContainer.innerHTML = imageByType.map(image => image.url).join('');
  } else {
    activePage = activePage + 1;
    const concatedImages = [];
    Object.keys(images).forEach(key => concatedImages.push(...images[key]));
    const imageByType = [...concatedImages].slice(0, activePage * perPage);
    imageContainer.innerHTML = imageByType.map(image => image.url).join('');
  }
}

loadMore.addEventListener('click', function () {
  document.querySelector('.load-more').style.display = 'none';
  document.querySelector('.container').style.display = 'block';
  setTimeout(function () {
    addimg(activeType);
    document.querySelector('.load-more').style.display = 'block';
    if (lodeQuantity === 3) {
      document.querySelector('.load-more').style.display = 'none';
    }
    document.querySelector('.container').style.display = 'none';
  }, 2000);
  ++lodeQuantity;
});

addimg(activeType);

const imagesPhoto = document.querySelectorAll('.images > li');

imagesPhoto.forEach(function (image) {
  let portfolioHover = document.querySelector('.portfolio-hover');
  image.addEventListener('mouseenter', function () {
    portfolioHover.style.opacity = '1';
    portfolioHover.style.transition = 'opacity 0.4s';
    this.appendChild(portfolioHover);
  });

  image.addEventListener('mouseleave', function () {
    portfolioHover.style.opacity = '0';
    this.classList.remove("portfolio-hover");
  });
});